package com.codeninjas.hackathon.network.users.repository


import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import com.codeninjas.hackathon.network.provider.MockUsersGlobalDataProvider
import io.reactivex.Observable
import javax.inject.Inject


class MockUsersRepository
@Inject
constructor(
    private val api: MockUsersGlobalDataProvider
) {

    fun getMockUsers(): Observable<MockUsersResponseModel>{
        return api.getMockUsers(0, 10)
    }
}
