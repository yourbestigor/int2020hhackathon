package com.codeninjas.hackathon.network.provider

import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MockUsersGlobalDataProvider {

    @GET("api/users")
    fun getMockUsers(@Query("page") page: Int = 0, @Query("per_page") perPage: Int): Observable<MockUsersResponseModel>
}