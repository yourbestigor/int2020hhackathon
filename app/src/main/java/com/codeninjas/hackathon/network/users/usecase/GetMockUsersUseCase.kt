package com.codeninjas.hackathon.network.users.usecase

import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import com.codeninjas.hackathon.network.users.repository.MockUsersRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetMockUsersUseCase
@Inject
constructor(private val repository: MockUsersRepository) {

    fun createObservable(): Observable<List<MockUsersResponseModel.Data>> {
        return repository.getMockUsers()
            .map {
                it.data
            }
    }
}