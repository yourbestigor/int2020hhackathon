package com.codeninjas.hackathon.ui.deposit

import android.os.Bundle
import android.view.View
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.wallet.*
import kotlinx.android.synthetic.main.fragment_create_habit.*
import ru.terrakok.cicerone.Router
import java.util.*
import javax.inject.Inject

@LayoutResourceId(R.layout.fragment_create_habit)
class ReloadDepositFragment : BaseFragment(){

    @Inject
    lateinit var router: Router

    private lateinit var paymentsClient: PaymentsClient
    private var isReadyToPay = false

    override fun renderView(view: View, savedInstanceState: Bundle?) {
        paymentsClient = getPaymentClient()
        isReadyToPay()
        btn_save_event.setOnClickListener {
            val request: PaymentDataRequest = createPayRequest(100.0)

            AutoResolveHelper.resolveTask(
                paymentsClient.loadPaymentData(request),
                activity!!,
                54321)
        }
    }

    private fun getPaymentClient(): PaymentsClient = Wallet.getPaymentsClient(activity!!,
        Wallet.WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_TEST)
            .build());


    private fun isReadyToPay() {
        val request = IsReadyToPayRequest.newBuilder()
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
            .build()
        val task = paymentsClient.isReadyToPay(request)
        task.addOnCompleteListener { task ->
            try {
                val result = task.getResult(ApiException::class.java)!!
                if (result) {
                    isReadyToPay = true
                }
            } catch (exception: ApiException) { }
        }
    }

    private fun createTokenizationParameters(): PaymentMethodTokenizationParameters {
        return PaymentMethodTokenizationParameters.newBuilder()
            .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
            .addParameter("gateway", "example")
            .addParameter("gatewayMerchantId", "exampleGatewayMerchantId")
            .build()
    }

    private fun createPayRequest(price: Double): PaymentDataRequest {
        val request = PaymentDataRequest.newBuilder()
            .setTransactionInfo(
                TransactionInfo.newBuilder()
                    .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                    .setTotalPrice(price.toString())
                    .setCurrencyCode("UAH")
                    .build())
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
            .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
            .setCardRequirements(
                CardRequirements.newBuilder()
                    .addAllowedCardNetworks(Arrays.asList(
                        WalletConstants.CARD_NETWORK_AMEX,
                        WalletConstants.CARD_NETWORK_DISCOVER,
                        WalletConstants.CARD_NETWORK_VISA,
                        WalletConstants.CARD_NETWORK_MASTERCARD))
                    .build())

        request.setPaymentMethodTokenizationParameters(createTokenizationParameters())
        return request.build()
    }


}