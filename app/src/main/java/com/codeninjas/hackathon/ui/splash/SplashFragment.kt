package com.codeninjas.hackathon.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import com.google.firebase.auth.FirebaseAuth
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@LayoutResourceId(R.layout.fragment_splash)
class SplashFragment : BaseFragment() {


    @Inject
    lateinit var router: Router

    override fun renderView(view: View, savedInstanceState: Bundle?) {
        activity!!.window!!.statusBarColor = ContextCompat.getColor(context!!, R.color.colorPrimary)
        Handler().postDelayed({
            if (FirebaseAuth.getInstance().currentUser == null) {
                router.newRootScreen(MainScreens.LogInScreen())
            } else router.newRootScreen(MainScreens.MainScreen())
        }, 2000)
    }
}