package com.codeninjas.hackathon.ui.settings

import android.Manifest
import android.os.Bundle
import android.view.View
import codeninjas.musicakinator.other.custom.extensions.showAlertMessage
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_settings.*
import pub.devrel.easypermissions.EasyPermissions
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@LayoutResourceId(R.layout.fragment_settings)
class SettingsFragment : BaseFragment() {

    @Inject
    lateinit var router: Router

    val RC_LOCATION = 12345

    override fun renderView(view: View, savedInstanceState: Bundle?) {
        tvAccessLocation.setOnClickListener {
            if (EasyPermissions.hasPermissions(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showAlertMessage("Доступ разрешен")
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    "Для правильной работы приложения необходимо предоставить разрешение на использование геолокации",
                    RC_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            }


        }

        tvExit.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            router.newRootScreen(MainScreens.LogInScreen())
        }

    }
}