package com.codeninjas.hackathon.ui.module

import com.codeninjas.hackathon.other.custom.annotations.PerFragment
import com.codeninjas.hackathon.ui.account.LogInFragment
import com.codeninjas.hackathon.ui.habit.create.CreateHabitFragment
import com.codeninjas.hackathon.ui.leaderboard.LeaderBoardFragment
import com.codeninjas.hackathon.ui.main.MainFragment
import com.codeninjas.hackathon.ui.profile.myProfile.MyProfileFragment
import com.codeninjas.hackathon.ui.settings.SettingsFragment
import com.codeninjas.hackathon.ui.splash.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentsBindingModule
{
    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindSplashScreen(): SplashFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindLeaderboardScreen(): LeaderBoardFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindMyProfileScreen(): MyProfileFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindCreateHabitScreen(): CreateHabitFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindMainScreen(): MainFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindSettingsScreen(): SettingsFragment

    @PerFragment
    @ContributesAndroidInjector
    abstract fun bindLogInScreen(): LogInFragment
}