package com.codeninjas.hackathon.ui.leaderboard

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.ui.leaderboard.adapter.LeaderBoardUserItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_leaderboard.*
import javax.inject.Inject

@LayoutResourceId(R.layout.fragment_leaderboard)
class LeaderBoardFragment : BaseFragment(), LeaderBoardView {

    @Inject
    @InjectPresenter
    lateinit var presenter: LeaderBoardPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    lateinit var itemAdapter: ItemAdapter<LeaderBoardUserItem>
    lateinit var adapter: FastAdapter<LeaderBoardUserItem>

    override fun renderView(view: View, savedInstanceState: Bundle?) {
        initUi()
        presenter.loadData()
    }

    private fun initUi() {
        itemAdapter = ItemAdapter()
        adapter = FastAdapter.Companion.with(itemAdapter)

        rv_mock_users.layoutManager = LinearLayoutManager(context!!)
        rv_mock_users.adapter = adapter
    }

    override fun onUsersLoaded(users: List<MockUsersResponseModel.Data>) {
        Glide.with(context!!).load(users[0].avatar).into(image_first_place)
        Glide.with(context!!).load(users[1].avatar).into(image_second_place)
        Glide.with(context!!).load(users[2].avatar).into(image_third_place)

        text_name_first_place.text = "${users[0].firstName} ${users[0].lastName}"
        text_name_second_place.text = "${users[1].firstName} ${users[1].lastName}"
        text_name_third_place.text = "${users[2].firstName} ${users[2].lastName}"

        val listUsers = users.subList(3, users.lastIndex)
        itemAdapter.clear()
        itemAdapter.add(listUsers.map { LeaderBoardUserItem((it)) }.toList())
    }
}