package com.codeninjas.hackathon.ui.leaderboard

import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import com.codeninjas.hackathon.other.base.BaseView

interface LeaderBoardView: BaseView {

    fun onUsersLoaded(users: List<MockUsersResponseModel.Data>)

}
