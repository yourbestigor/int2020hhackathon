package com.codeninjas.hackathon.ui.habit.create

import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import codeninjas.musicakinator.other.custom.extensions.drawSimpleSelectorDialog
import codeninjas.musicakinator.other.custom.extensions.parseCurrencyString
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.domain.habit.Habit
import com.codeninjas.hackathon.domain.habit.event.OnHabitCreatedEvent
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetView
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.fragment_create_habit.*
import org.greenrobot.eventbus.EventBus
import ru.terrakok.cicerone.Router
import java.util.*
import javax.inject.Inject

@LayoutResourceId(R.layout.fragment_create_habit)
class CreateHabitFragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var router: Router

    private lateinit var dp: DatePickerDialog

    @Inject
    lateinit var pref: SharedPreferences

    override fun renderView(view: View, savedInstanceState: Bundle?) {
        initDatePicker()
        val categories = listOf("Улучшение качества питания", "Увеличить физическую активность")
        text_input_habit_type.setOnClickListener {
            drawSimpleSelectorDialog("Выберите категорию", categories) { dialogInterface: DialogInterface, i: Int ->
                text_input_habit_type.text = categories[i]
            }
        }
        btn_save_event.setOnClickListener {
            //pref["mock_habit"] = true
            EventBus.getDefault().postSticky(OnHabitCreatedEvent(
                Habit(
                    edt_input_habit_title.text.toString(),
                    text_input_habit_start_date.text.toString(),
                    text_input_habit_type.text.toString(),
                    edt_input_habit_sum.text.toString().parseCurrencyString()
                    )
            ))
            router.backTo(MainScreens.MainScreen())

        }
        showTutorial()
    }


    private fun showTutorial() {
        TapTargetView.showFor(activity!!,
            TapTarget.forView(
               edt_input_habit_sum,
                "Сумма спора",
                "Введите сумму которая будет зарезервирована для участия в споре. В случае успешного установления привычки вы сможете вернуть деньги"
            )
                .outerCircleColor(R.color.colorPrimary)
                .outerCircleAlpha(0.96f)
                .targetCircleColor(R.color.white)
                .titleTextSize(20)
                .titleTextColor(R.color.white)
                .descriptionTextSize(14)
                .descriptionTextColor(R.color.white)
                .textTypeface(Typeface.SANS_SERIF)
                .drawShadow(true)
                .cancelable(true)
                .tintTarget(true)
                .transparentTarget(true)
                .targetRadius(60),
            object : TapTargetView.Listener() {
            })
    }

    private fun initDatePicker() {
        val now: Calendar = Calendar.getInstance()
        dp = DatePickerDialog.newInstance(
            this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        )
        dp.minDate = now
        dp.accentColor = ContextCompat.getColor(context!!, R.color.colorPrimary)
        dp.version = DatePickerDialog.Version.VERSION_2
        text_input_habit_start_date.setOnClickListener {
            dp.show(fragmentManager!!, "Datepickerdialog")
        }
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        text_input_habit_start_date.text = "$dayOfMonth/$monthOfYear/$year"
    }
}