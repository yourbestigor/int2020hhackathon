package com.codeninjas.hackathon.ui.main

import android.os.Bundle
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.other.base.BaseActivity
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

@LayoutResourceId(R.layout.activity_main)
class MainActivity : BaseActivity() {

    private val navigator = SupportAppNavigator(this, R.id.container)

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    override fun renderView(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            router.newRootScreen(MainScreens.SplashScren())
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

}
