package com.codeninjas.hackathon.ui.main

import android.os.Bundle
import android.view.View
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import kotlinx.android.synthetic.main.fragment_main.*
import ru.terrakok.cicerone.android.support.SupportAppScreen

@LayoutResourceId(R.layout.fragment_main)
class MainFragment : BaseFragment() {

    private val currentTabFragment: BaseFragment?
        get() = childFragmentManager.fragments.firstOrNull { !it.isHidden } as? BaseFragment


    override fun renderView(view: View, savedInstanceState: Bundle?) {
        initUi()
    }

    private fun initUi() {
        bottom_nav_view.setOnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.navigation_dashboard -> {
                    selectTab(dashboard)
                    true
                }
                R.id.navigation_profile -> {
                    selectTab(profile)
                    true
                }
                R.id.navigation_settings -> {
                    selectTab(settings)
                    true
                }
                else -> false
            }
        }
        if (currentTabFragment == null) {
            bottom_nav_view.selectedItemId = R.id.navigation_dashboard
            selectTab(dashboard)
        }

    }

    private fun selectTab(tab: SupportAppScreen) {
        val currentFragment = currentTabFragment
        val newFragment = childFragmentManager.findFragmentByTag(tab.screenKey)

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) return

        childFragmentManager.beginTransaction().apply {
            if (newFragment == null) add(R.id.main_container, createTabFragment(tab), tab.screenKey)

            currentFragment?.let {
                hide(it)
                it.userVisibleHint = false
            }
            newFragment?.let {
                show(it)
                it.userVisibleHint = true
            }
        }.commitNow()
    }

    private fun createTabFragment(tab: SupportAppScreen) = tab.fragment


    companion object {
        private val dashboard = MainScreens.LeaderBoardScreen()
        private val profile = MainScreens.MyProfileScreen()
        private val settings = MainScreens.SettingsScreen()
    }


}