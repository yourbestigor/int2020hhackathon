package com.codeninjas.hackathon.ui.leaderboard

import codeninjas.musicakinator.other.custom.extensions.async
import com.arellomobile.mvp.InjectViewState
import com.codeninjas.hackathon.network.users.usecase.GetMockUsersUseCase
import com.codeninjas.hackathon.other.base.BasePresenter
import com.codeninjas.hackathon.other.custom.annotations.PerFragment
import javax.inject.Inject

@PerFragment
@InjectViewState
class LeaderBoardPresenter
@Inject
constructor(private val getMockUsersUseCase: GetMockUsersUseCase) :
    BasePresenter<LeaderBoardView>() {

    fun loadData() {
        getMockUsersUseCase.createObservable()
            .async()
            .doOnSubscribe { viewState.showProgress() }
            .doOnTerminate { viewState.hideProgress() }
            .subscribe({

                viewState.onUsersLoaded(it)
            }, {}).tracked()
    }


}
