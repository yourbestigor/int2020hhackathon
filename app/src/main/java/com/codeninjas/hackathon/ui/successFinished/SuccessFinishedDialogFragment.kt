package com.codeninjas.hackathon.ui.successFinished

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.codeninjas.hackathon.R
import kotlinx.android.synthetic.main.detect_mistake_dialog.*


class SuccessFinishedDialogFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.success_finished_dialog, container, false)
        val displayRectangle = Rect()
        val window = dialog!!.window
        window!!.decorView.getWindowVisibleDisplayFrame(displayRectangle)

        view.minimumWidth = (displayRectangle.width() * 0.9f).toInt()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvCancel.setOnClickListener {
            dismiss()
        }

        tvConfirm.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        fun newInstance(): SuccessFinishedDialogFragment = SuccessFinishedDialogFragment()
    }
}