/*
 * Created by Igor Shyian on 2/5/20 3:15 PM
 * Fruktorum (c) 2020 . All rights reserved.
 */

package com.codeninjas.hackathon.ui.leaderboard.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.domain.mockUsers.responseModel.MockUsersResponseModel
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_dashboard_mock_user.view.*

class LeaderBoardUserItem(val user: MockUsersResponseModel.Data) :
    AbstractItem<LeaderBoardUserItem.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.item_dashboard_mock_user

    override val type: Int
        get() = R.id.item_dashboard_mock_user

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    inner class ViewHolder(view: View) : FastAdapter.ViewHolder<LeaderBoardUserItem>(view) {

        override fun bindView(item: LeaderBoardUserItem, payloads: MutableList<Any>) {
            Glide.with(itemView.context).load(item.user.avatar).into(itemView.image_mock_user_avatar)
            itemView.text_mock_user_name.text = "${item.user.firstName} ${item.user.lastName}"
            itemView.text_rating_place.text = item.user.id.toString()
            itemView.text_user_rating.text = (1800 - item.user.id * 42).toString()
        }

        override fun unbindView(item: LeaderBoardUserItem) {

        }
    }
}