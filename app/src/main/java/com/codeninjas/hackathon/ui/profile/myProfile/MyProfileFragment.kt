package com.codeninjas.hackathon.ui.profile.myProfile

import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import codeninjas.musicakinator.other.custom.extensions.gone
import codeninjas.musicakinator.other.custom.extensions.parseCurrencyString
import codeninjas.musicakinator.other.custom.extensions.visible
import com.bumptech.glide.Glide
import com.codeninjas.hackathon.R
import com.codeninjas.hackathon.domain.habit.event.OnHabitCreatedEvent
import com.codeninjas.hackathon.other.base.BaseFragment
import com.codeninjas.hackathon.other.custom.annotations.LayoutResourceId
import com.codeninjas.hackathon.other.screens.MainScreens
import com.codeninjas.hackathon.ui.successFinished.SuccessFinishedDialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.sheet_habit_dialog.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@LayoutResourceId(R.layout.fragment_my_profile)
class MyProfileFragment : BaseFragment() {

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var pref: SharedPreferences


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun renderView(view: View, savedInstanceState: Bundle?) {


        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            Glide.with(context!!).load(currentUser.photoUrl!!).into(ivUserAvatar)
            tvFullName.text = currentUser.displayName
            tvUserName.text = currentUser.displayName
        }
        btnCreateHabit.setOnClickListener {
            router.navigateTo(MainScreens.CreateHabitScreen())
        }

        tvDepositValue.setOnClickListener {
            val dialog = getReloadDepositDialog()
            dialog.show()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        }


    }


    fun getReloadDepositDialog(
    ): BottomSheetDialog {

        val bottomSheetDialog = BottomSheetDialog(context!!, R.style.BottomSheetDialog)
        val dialogView = layoutInflater.inflate(R.layout.sheet_habit_dialog, null)
        bottomSheetDialog.setContentView(dialogView)
        bottomSheetDialog.setCancelable(false)
        dialogView.dialog_btn_continue.setOnClickListener {
            if (dialogView.edt_input_habit_sum2.text!!.isNotEmpty()) {
                val sum = dialogView.edt_input_habit_sum2.text.toString().parseCurrencyString()
                tvDepositValue.text = "${sum.toInt() + 132} $"
                bottomSheetDialog.dismiss()
            }
        }
        return bottomSheetDialog

    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessageEvent(event: OnHabitCreatedEvent?) {
        clHabit.visible()
        habitPlaceholder.gone()
        tvHabitTitle.text = event!!.habit.title
        text_habit_start_date.text = event.habit.date
        text_habit_sum.text = event.habit.sum.toString() + "$"
        text_habit_category.text = event.habit.category

        Handler().postDelayed({
            //DetectMistakeDialogFragment.newInstance().show(childFragmentManager, "detect_mistakes")
            SuccessFinishedDialogFragment.newInstance()
                .show(childFragmentManager, "success_finished")
        }, 5000)
        EventBus.getDefault().removeStickyEvent(event)
    }


}