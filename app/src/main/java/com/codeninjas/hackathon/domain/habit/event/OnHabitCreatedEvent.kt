package com.codeninjas.hackathon.domain.habit.event

import com.codeninjas.hackathon.domain.habit.Habit

class OnHabitCreatedEvent(val habit: Habit) {
}