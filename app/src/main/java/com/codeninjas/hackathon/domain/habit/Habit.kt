package com.codeninjas.hackathon.domain.habit

data class Habit(val title: String, val date: String, val category: String, val sum: Double) {
}