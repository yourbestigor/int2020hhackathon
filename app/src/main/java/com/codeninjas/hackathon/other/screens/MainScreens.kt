package com.codeninjas.hackathon.other.screens

import androidx.fragment.app.Fragment
import com.codeninjas.hackathon.ui.account.LogInFragment
import com.codeninjas.hackathon.ui.habit.create.CreateHabitFragment
import com.codeninjas.hackathon.ui.leaderboard.LeaderBoardFragment
import com.codeninjas.hackathon.ui.main.MainFragment
import com.codeninjas.hackathon.ui.profile.myProfile.MyProfileFragment
import com.codeninjas.hackathon.ui.settings.SettingsFragment
import com.codeninjas.hackathon.ui.splash.SplashFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class MainScreens {

    class SplashScren(): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return SplashFragment()
        }
    }



    class LeaderBoardScreen(): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return LeaderBoardFragment()
        }
    }

    class MyProfileScreen: SupportAppScreen() {
        override fun getFragment(): Fragment {
            return MyProfileFragment()
        }
    }

    class SettingsScreen: SupportAppScreen() {
        override fun getFragment(): Fragment {
            return SettingsFragment()
        }
    }

    class CreateHabitScreen(): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return CreateHabitFragment()
        }
    }

    class MainScreen(): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return MainFragment()
        }
    }

    class LogInScreen(): SupportAppScreen(){
        override fun getFragment(): Fragment {
            return LogInFragment()
        }
    }
}