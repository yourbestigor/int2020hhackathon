package com.codeninjas.hackathon.di.modules

import com.codeninjas.hackathon.network.provider.MockUsersGlobalDataProvider
import com.codeninjas.hackathon.other.custom.constants.ApiConstants
import dagger.Module
import dagger.Provides

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class ApiMockUsersModule {

    @Singleton
    @Provides
    @Named("BASE_URL_USERS_API")
    fun provideBaseUrl(): String {
        return ApiConstants.BASE_URL_MOCK_USERS
    }


    @Singleton
    @Provides
    @Named("provideRetrofitApi")
    fun provideRetrofit(
        @Named("BASE_URL_USERS_API") baseUrl: String,
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .addConverterFactory(gsonConverterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiService(@Named("provideRetrofitApi") retrofit: Retrofit): MockUsersGlobalDataProvider {
        return retrofit.create(MockUsersGlobalDataProvider::class.java)
    }
}