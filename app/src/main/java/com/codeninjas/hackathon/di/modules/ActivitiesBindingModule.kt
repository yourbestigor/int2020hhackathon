package com.codeninjas.hackathon.di.modules


import com.codeninjas.hackathon.other.custom.annotations.PerActivity
import com.codeninjas.hackathon.ui.main.MainActivity
import com.codeninjas.hackathon.ui.module.MainFragmentsBindingModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesBindingModule {

    @PerActivity
    @ContributesAndroidInjector(
        modules = [MainFragmentsBindingModule::class]
    )
    abstract fun bindMainActivity(): MainActivity


}